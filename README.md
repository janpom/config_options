# Approaches to app configuration

Various approaches to app configuration demonstrated on a simple API and it's usage in a simple 
application.

## Example 1

#### Pros

- in the API it's very clear what the defaults are
  - no need for any additional documentation of the defaults 
- the default values are easy to access from the API
- tests easy to write

#### Cons

- lot of redundancy - names of configurations options and their description repeated
  many times in the code
- doing the API calls from the `example1/tax_calculator_app.py` is more work than it could be

#### Summary

This approach keeps the API clean and makes it easy to maintain the API code.
However, this comes at the cost of redundancy and extra work outside of the API.


## Example 2

#### Pros compared to Example 1

- API easier to call
- simpler `TaxCalculator.__init__`

#### Cons compared to Example 1

- API implementation more complicated
  - extra work required for getting default method parameter values:
    - `expense_ratio = fallback(expense_ratio, self._config['expense_ratio'])`
  - more difficult to reference the config values (
    `self._config['tax_ratio']` vs `self._tax_ratio`)
    - no PyCharm auto-completion/syntax-check for the config values
- clunkier API documentation (config has to be referenced to explain the default method parameter
  values; within the method documentation context, it may not be clear what the config is)
- testing slightly more complicated
  - slightly more boilerplate
  - no PyCharm auto-completion for specifying the config options (creating a dictionary
    vs. providing `__init__` parameters)
