from example2.defaults import TAX_CALCULATOR_DEFAULTS
from example2.tax_calculator_api import TaxCalculator


def test_default_tax_calculator():
    tc = TaxCalculator(TAX_CALCULATOR_DEFAULTS)
    assert tc.calculate_tax(1000000) == 35000.0
    assert tc.calculate_tax(1000000, expense_ratio=0.5) == 50000.0
    assert tc.calculate_tax(1000000, is_student=True) == 10000.0
    assert tc.calculate_tax(1000000, expense_ratio=0.5, is_student=True) == 25000.0


def test_configured_tax_calculator():
    config = dict(
        TAX_CALCULATOR_DEFAULTS, tax_ratio=0.2, tax_discount=10000, student_tax_discount=0)
    tc = TaxCalculator(config)
    assert tc.calculate_tax(1000000) == 70000.0
    assert tc.calculate_tax(1000000, expense_ratio=0.5) == 90000.0
    assert tc.calculate_tax(1000000, is_student=True) == 60000.0
    assert tc.calculate_tax(1000000, expense_ratio=0.5, is_student=True) == 80000.0
