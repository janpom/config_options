TAX_CALCULATOR_DEFAULTS = dict(
    # percentage of (income - expense) that makes up the tax
    tax_ratio=0.15,

    # amount deduced from the calculated tax for all tax payers
    tax_discount=25000,

    # additional amount deduced from the calculated tax for students
    student_tax_discount=10000,

    # expenses as the percentage of the income
    expense_ratio=0.6,

    # is the tax payer a student?
    is_student=False,
)
