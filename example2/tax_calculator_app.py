import configparser
from typing import Any, Dict

from example2.defaults import TAX_CALCULATOR_DEFAULTS
from example2.tax_calculator_api import TaxCalculator


def load_config(ini_file_path: str) -> Dict[str, Any]:
    config_ini = configparser.ConfigParser()
    config_ini.read(ini_file_path)
    main_section = config_ini['tax_calculator_defaults']

    return {
        'tax_ratio': main_section.getfloat(
            'TAX_RATIO', TAX_CALCULATOR_DEFAULTS['tax_ratio']),

        'tax_discount': main_section.getint(
            'TAX_DISCOUNT', TAX_CALCULATOR_DEFAULTS['tax_discount']),

        'student_tax_discount': main_section.getint(
            'STUDENT_TAX_DISCOUNT', TAX_CALCULATOR_DEFAULTS['student_tax_discount']),

        'expense_ratio': main_section.getfloat(
            'EXPENSE_RATIO', TAX_CALCULATOR_DEFAULTS['expense_ratio']),

        'is_student': main_section.getboolean(
            'IS_STUDENT', TAX_CALCULATOR_DEFAULTS['is_student']),
    }


if __name__ == "__main__":
    config = load_config("config.ini")
    print(config)

    tax_calculator = TaxCalculator(config)

    income = 1000000
    tax = tax_calculator.calculate_tax(income)
    print("tax(%.1f) == %.1f" % (income, tax))
