from typing import Any, Dict

from util import fallback


class TaxCalculator(object):
    """
    Income tax calculator.
    """

    def __init__(
            self,
            config: Dict[str, Any],
            ):
        """
        Initialize and configure the tax calculator.

        Args:
            config: dictionary expected to contain the following attributes
            - tax_ratio (float): percentage of (income - expense) that makes up the tax
            - tax_discount (int): amount deduced from the calculated tax
            - student_tax_discount (bool): is the tax payer a student?
            ... and further attributes representing the method defaults
        """
        self._config = config

    def calculate_tax(
            self,
            income: float,
            expense_ratio: float = None,
            is_student: bool = None,
            ):
        """
        Calculate income tax.

        Args:
            income: gross income
            expense_ratio: expenses as the percentage of the income; defaults to
                `config['expense_ratio']`
            is_student: is the tax payer a student?; defaults to `config['is_student']`
        """
        expense_ratio = fallback(expense_ratio, self._config['expense_ratio'])
        is_student = fallback(is_student, self._config['is_student'])

        expense = income * expense_ratio
        tax = (income - expense) * self._config['tax_ratio'] - self._config['tax_discount']
        if is_student:
            tax -= self._config['tax_discount']
        return max(tax, 0)
