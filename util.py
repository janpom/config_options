def fallback(value, default_value):
    if value is not None:
        return value
    else:
        return default_value
