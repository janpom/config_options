from example1.defaults import EXPENSE_RATIO, IS_STUDENT, STUDENT_TAX_DISCOUNT, TAX_DISCOUNT, \
    TAX_RATIO


class TaxCalculator(object):
    """
    Income tax calculator.
    """

    def __init__(
            self,
            tax_ratio: float = TAX_RATIO,
            tax_discount: int = TAX_DISCOUNT,
            student_tax_discount: int = STUDENT_TAX_DISCOUNT,
            ):
        """
        Initialize and configure the tax calculator.

        Args:
            tax_ratio: percentage of (income - expense) that makes up the tax
            tax_discount: amount deduced from the calculated tax
            student_tax_discount: is the tax payer a student?
        """
        self._tax_ratio = tax_ratio
        self._tax_discount = tax_discount
        self._student_tax_discount = student_tax_discount

    def calculate_tax(
            self,
            income: float,
            expense_ratio: float = EXPENSE_RATIO,
            is_student: bool = IS_STUDENT,
            ):
        """
        Calculate income tax.

        Args:
            income: gross income
            expense_ratio: expenses as the percentage of the income
            is_student: is the tax payer a student?
        """
        expense = income * expense_ratio
        tax = (income - expense) * self._tax_ratio - self._tax_discount
        if is_student:
            tax -= self._tax_discount
        return max(tax, 0)
