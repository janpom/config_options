import configparser
from typing import Any, Dict

from example1.defaults import EXPENSE_RATIO, IS_STUDENT, STUDENT_TAX_DISCOUNT, TAX_DISCOUNT, \
    TAX_RATIO
from example1.tax_calculator_api import TaxCalculator


def load_config(ini_file_path: str) -> Dict[str, Any]:
    config_ini = configparser.ConfigParser()
    config_ini.read(ini_file_path)
    main_section = config_ini['tax_calculator_defaults']

    return {
        'TAX_RATIO': main_section.getfloat('TAX_RATIO', TAX_RATIO),
        'TAX_DISCOUNT': main_section.getint('TAX_DISCOUNT', TAX_DISCOUNT),
        'STUDENT_TAX_DISCOUNT': main_section.getint('STUDENT_TAX_DISCOUNT', STUDENT_TAX_DISCOUNT),
        'EXPENSE_RATIO': main_section.getfloat('EXPENSE_RATIO', EXPENSE_RATIO),
        'IS_STUDENT': main_section.getboolean('IS_STUDENT', IS_STUDENT),
    }


if __name__ == "__main__":
    config = load_config("config.ini")
    print(config)

    tax_calculator = TaxCalculator(
        tax_ratio=config['TAX_RATIO'],
        tax_discount=config['TAX_DISCOUNT'],
        student_tax_discount=config['STUDENT_TAX_DISCOUNT'],
    )

    income = 1000000
    tax = tax_calculator.calculate_tax(income, config['EXPENSE_RATIO'], config['IS_STUDENT'])
    print("tax(%.1f) == %.1f" % (income, tax))
