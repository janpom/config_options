# percentage of (income - expense) that makes up the tax
TAX_RATIO = 0.15

# amount deduced from the calculated tax for all tax payers
TAX_DISCOUNT = 25000

# additional amount deduced from the calculated tax for students
STUDENT_TAX_DISCOUNT = 10000

# expenses as the percentage of the income
EXPENSE_RATIO = 0.6

# is the tax payer a student?
IS_STUDENT = False
